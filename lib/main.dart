import 'package:chat_ui/pages/home.dart';
import 'package:chat_ui/styles/color_schemes.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat UI',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: lightColorScheme,
        backgroundColor: lightColorScheme.background,
        textTheme: GoogleFonts.interTextTheme()
      ),
      home: const HomeScreen(),
    );
  }
}

import 'package:chat_ui/material_components/cards/filled_card.dart';
import 'package:chat_ui/material_components/extensions/context_extensions.dart';
import 'package:chat_ui/models/chat_detail_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MessageBubbleMe extends StatelessWidget {
  final ChatDetailModel item;
  const MessageBubbleMe({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        right: 10,
        bottom: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          AlphaFilledCard(
            color: context.primaryColor,
            margin: const EdgeInsets.only(left: 50, bottom: 5, right: 10),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    item.message,
                    textAlign: TextAlign.left,
                    style:
                        context.getLabelMediumTextStyle(context.onPrimaryColor),
                  ),
                  Text(
                    DateFormat("dd MMM yy - HH:MM").format(item.timestamp),
                    textAlign: TextAlign.left,
                    style: context
                        .getLabelSmallTextStyle(
                            context.onPrimaryColor.withOpacity(0.9))
                        ?.copyWith(fontSize: 9),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MessageBubbleOther extends StatelessWidget {
  final ChatDetailModel item;
  const MessageBubbleOther({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: 10,
        bottom: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AlphaFilledCard(
            color: context.disabledColor.withOpacity(0.05),
            margin: const EdgeInsets.only(right: 50, bottom: 5, left: 10),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    item.message,
                    textAlign: TextAlign.left,
                    style:
                        context.getLabelMediumTextStyle(context.onSurfaceColor),
                  ),
                  Text(
                    DateFormat("dd MMM yy - HH:MM").format(item.timestamp),
                    textAlign: TextAlign.left,
                    style: context
                        .getLabelSmallTextStyle(context.unSelectedColor)
                        ?.copyWith(fontSize: 9),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:chat_ui/material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../material_components/cards/filled_card.dart';
import '../material_components/image/network_image.dart';
import '../models/chat_list_model.dart';
import '../pages/chat_detail.dart';

class ItemChat extends StatelessWidget {
  final ChatListModel item;
  const ItemChat({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    String imageUrl =
        "https://ui-avatars.com/api/?name=${item.sender}&background=random";
    return AlphaFilledCard(
      margin: const EdgeInsets.only(bottom: 15),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const ChatDetailScreen(),
            ),
          );
        },
        borderRadius: BorderRadius.circular(12),
        child: ListTile(
          leading: SizedBox(
            width: 40,
            child: AlphaNetworkImage(imageUrl),
          ),
          title: Text(
            item.sender,
            style: context.getBodyLargeTextStyle(context.onSurfaceColor),
          ),
          subtitle: Text(
            item.message,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: context
                .getLabelMediumTextStyle(
                    item.isRead ? context.outlineColor : context.onSurfaceColor)
                ?.copyWith(
                    fontWeight:
                        item.isRead ? FontWeight.normal : FontWeight.bold),
          ),
          trailing: Text(
            DateFormat("dd MMM yy").format(item.timestamp),
            style: context.getLabelSmallTextStyle(context.outlineColor),
          ),
        ),
      ),
    );
  }
}

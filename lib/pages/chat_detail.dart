import 'dart:math';

import 'package:chat_ui/material_components/appbar/app_bar.dart';
import 'package:chat_ui/material_components/buttons/icon_button.dart';
import 'package:chat_ui/material_components/buttons/text_button.dart';
import 'package:chat_ui/material_components/cards/filled_card.dart';
import 'package:chat_ui/material_components/extensions/context_extensions.dart';
import 'package:chat_ui/material_components/text_field/form_text_field.dart';
import 'package:chat_ui/models/chat_detail_model.dart';
import 'package:chat_ui/widgets/message_bubble.dart';
import 'package:flutter/material.dart';

class ChatDetailScreen extends StatefulWidget {
  const ChatDetailScreen({super.key});

  @override
  State<ChatDetailScreen> createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  final textController = TextEditingController();
  ScrollController _scrollController = new ScrollController();
  double paddingBottom = 0;
  List<ChatDetailModel> chatDetail = [
    ChatDetailModel(
      message: "Hi, how are you?",
      timestamp: DateTime.now().subtract(
        Duration(
          days: Random().nextInt(100),
          hours: Random().nextInt(24),
        ),
      ),
      isMySelf: true,
    ),
    ChatDetailModel(
      message: "I'm Jack Sparrow. What is your name?",
      timestamp: DateTime.now().subtract(
        Duration(
          days: Random().nextInt(100),
          hours: Random().nextInt(24),
        ),
      ),
      isMySelf: true,
    ),
    ChatDetailModel(
      message: "Hi Jack, I'm Scarlett Witch.",
      timestamp: DateTime.now().subtract(
        Duration(
          days: Random().nextInt(100),
          hours: Random().nextInt(24),
        ),
      ),
      isMySelf: false,
    ),
    ChatDetailModel(
      message: "Nice to meet you, Scarlett. Where are you from?",
      timestamp: DateTime.now().subtract(
        Duration(
          days: Random().nextInt(100),
          hours: Random().nextInt(24),
        ),
      ),
      isMySelf: true,
    ),
    ChatDetailModel(
      message: "I am from Marvel Cinematic Universe.",
      timestamp: DateTime.now().subtract(
        Duration(
          days: Random().nextInt(100),
          hours: Random().nextInt(24),
        ),
      ),
      isMySelf: false,
    ),
  ];

  void addNewChat() {
    FocusManager.instance.primaryFocus?.unfocus();
    if (textController.text.toString().isNotEmpty) {
      setState(() {
        chatDetail.add(
          ChatDetailModel(
            message: textController.text.toString(),
            timestamp: DateTime.now(),
            isMySelf: true,
          ),
        );
        textController.clear();
      });
      scrollToBottom();
    }
  }

  scrollToBottom() {
    setState(() {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AlphaAppBar(
        title: "Chat Detail",
      ),
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AlphaIconButton(
                    onPressed: () {},
                    color: context.unSelectedColor,
                    icon: Icons.image,
                  ),
                  Expanded(
                    child: AlphaFilledCard(
                      color: context.disabledColor.withOpacity(0.07),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: AlphaTextFormField(
                          controller: textController,
                          textCapitalization: TextCapitalization.sentences,
                          useBorder: false,
                          hintText: "Type your message here...",
                          maxLines: 3,
                          onChanged: ((value) {
                            if (_scrollController.offset !=
                                _scrollController.position.maxScrollExtent) {
                              scrollToBottom();
                            }
                          }),
                          suffixIcon: AlphaTextButton(
                            onPressed: () {
                              addNewChat();
                            },
                            child: "Send",
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      body: ListView.builder(
        controller: _scrollController,
        itemCount: chatDetail.length,
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).size.height * 0.1,
        ),
        itemBuilder: (context, index) {
          var item = chatDetail[index];
          return Builder(
            builder: (context) {
              if (item.isMySelf) {
                return MessageBubbleMe(item: item);
              }
              return MessageBubbleOther(item: item);
            },
          );
        },
      ),
    );
  }
}

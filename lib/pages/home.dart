import 'package:chat_ui/material_components/appbar/app_bar.dart';
import 'package:chat_ui/material_components/cards/filled_card.dart';
import 'package:chat_ui/material_components/extensions/context_extensions.dart';
import 'package:chat_ui/material_components/image/network_image.dart';
import 'package:chat_ui/material_components/snackbar/snackbar.dart';
import 'package:chat_ui/models/chat_list_model.dart';
import 'package:chat_ui/widgets/list_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'chat_detail.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AlphaAppBar(
        title: "Chat List",
      ),
      floatingActionButton: Container(
        margin: const EdgeInsets.only(
          bottom: 15,
          right: 15,
        ),
        child: FloatingActionButton.extended(
          onPressed: () {
            showSnackbar(context, child: "Halooo!", isFloating: false);
          },
          elevation: 1,
          label: const Text("New chat"),
          icon: const Icon(Icons.chat),
        ),
      ),
      body: ListView.builder(
        itemCount: chatList.length,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        itemBuilder: (context, index) {
          var item = chatList[index];
          return ItemChat(item: item);
        },
      ),
    );
  }
}

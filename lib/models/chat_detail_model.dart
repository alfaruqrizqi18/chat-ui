import 'dart:math';

class ChatDetailModel {
  String message;
  DateTime timestamp;
  bool isMySelf;

  ChatDetailModel({
    required this.message,
    required this.timestamp,
    required this.isMySelf,
  });
}

import 'dart:math';

class ChatListModel {
  String sender;
  String message;
  DateTime timestamp;
  bool isRead;

  ChatListModel({
    required this.sender,
    required this.message,
    required this.timestamp,
    required this.isRead,
  });
}

List<ChatListModel> chatList = [
  ChatListModel(
    sender: "Monalisa",
    message: "Hi, I'm monalisa. Who are you?",
    isRead: true,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Dr. Strange",
    message: "Dr. Strange here, what can I help you?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Iron Man",
    message: "Do you want to join with us in Avenger?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Dr. Strange",
    message: "Dr. Strange here, what can I help you?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Monalisa",
    message: "Hi, I'm monalisa. Who are you?",
    isRead: true,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Dr. Strange",
    message: "Dr. Strange here, what can I help you?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Iron Man",
    message: "Do you want to join with us in Avenger?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),ChatListModel(
    sender: "Monalisa",
    message: "Hi, I'm monalisa. Who are you?",
    isRead: true,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Dr. Strange",
    message: "Dr. Strange here, what can I help you?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
  ChatListModel(
    sender: "Iron Man",
    message: "Do you want to join with us in Avenger?",
    isRead: false,
    timestamp: DateTime.now().subtract(
      Duration(
        days: Random().nextInt(100),
        hours: Random().nextInt(24),
      ),
    ),
  ),
];

// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaElevatedCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry? margin;

  const AlphaElevatedCard({Key? key, required this.child, this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      surfaceTintColor: context.surfaceColor,
      margin: margin,
      child: child,
      elevation: 1,
      shadowColor: context.shadowColor,
    );
  }
}

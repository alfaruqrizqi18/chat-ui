import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaFilledCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry? margin;
  final Color? color;

  const AlphaFilledCard({Key? key, required this.child, this.margin, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: margin,
      color: color ?? context.surfaceVariantColor,
      child: child,
    );
  }
}

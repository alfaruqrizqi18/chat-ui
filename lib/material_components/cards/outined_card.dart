import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaOutlinedCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry? margin;

  const AlphaOutlinedCard({Key? key, required this.child, this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: margin,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: context.outlineColor,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
      ),
      child: child,
    );
  }
}

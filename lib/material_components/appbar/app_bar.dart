import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaAppBar extends StatelessWidget implements PreferredSizeWidget {
  final dynamic title;
  final Widget? leading;
  final List<Widget>? actions;
  final bool automaticallyImplyLeading;
  final PreferredSizeWidget? bottom;
  final bool centerTitle;
  final double paddingRightActionButtons;

  const AlphaAppBar({
    Key? key,
    this.title,
    this.leading,
    this.actions,
    this.automaticallyImplyLeading = true,
    this.bottom,
    this.centerTitle = true,
    this.paddingRightActionButtons = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      surfaceTintColor: context.surfaceTintColor,
      backgroundColor: context.surfaceColor,
      iconTheme: IconThemeData(color: context.onSurfaceColor, size: 24),
      actionsIconTheme: IconThemeData(color: context.onSurfaceVariantColor),
      bottom: bottom,
      title: Builder(
        builder: (ctx) {
          if (title is String) {
            return Text(
              title,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: context
                  .getTitleLargeTextStyle(context.onSurfaceColor)
                  ?.copyWith(fontWeight: FontWeight.bold, fontSize: 18),
            );
          }
          if (title is Widget) {
            return title;
          }
          return const SizedBox();
        },
      ),
      scrolledUnderElevation: 1,
      shadowColor: context.shadowColor,
      leading: leading,
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: actions ?? [],
          ),
        )
      ],
      centerTitle: true,
      automaticallyImplyLeading: automaticallyImplyLeading,
    );
  }

  @override
  Size get preferredSize {
    if (bottom == null) {
      return const Size.fromHeight(64);
    }
    return Size.fromHeight(bottom!.preferredSize.height);
  }
}

Widget appBarCartIcon(BuildContext context) {
  return GestureDetector(
    onTap: () {},
    child: Icon(
      Icons.shopping_cart,
      color: context.primaryColor,
    ),
  );
}

PreferredSizeWidget AlphaAppBarV2(
  BuildContext context, {
  final dynamic title,
  final Widget? leading,
  final List<Widget>? actions,
  final bool automaticallyImplyLeading = true,
  final PreferredSizeWidget? bottom,
}) {
  return AppBar(
    surfaceTintColor: context.surfaceTintColor,
    backgroundColor: context.surfaceColor,
    iconTheme: IconThemeData(color: context.onSurfaceColor, size: 24),
    actionsIconTheme: IconThemeData(color: context.onSurfaceVariantColor),
    bottom: bottom,
    title: Builder(
      builder: (ctx) {
        if (title is String) {
          return Text(
            title,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: context
                .getTitleLargeTextStyle(context.onSurfaceColor)
                ?.copyWith(fontWeight: FontWeight.bold, fontSize: 18),
          );
        }
        if (title is Widget) {
          return title;
        }
        return const SizedBox();
      },
    ),
    scrolledUnderElevation: 1,
    shadowColor: context.shadowColor,
    leading: leading,
    actions: [
      Padding(
        padding: const EdgeInsets.only(right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: actions ?? [],
        ),
      )
    ],
    centerTitle: true,
    automaticallyImplyLeading: automaticallyImplyLeading,
  );
}

import '../../material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';

void showSnackbar(
  BuildContext context, {
  required dynamic child,
  String? actionLabel,
  Function? onPressedAction,
  Duration duration = const Duration(seconds: 3),
  bool isFloating = true,
}) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      behavior: isFloating ? SnackBarBehavior.floating : SnackBarBehavior.fixed,
      duration: duration,
      content: Builder(
        builder: (context) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
      action: SnackBarAction(
        label: actionLabel ?? "",
        onPressed: () {
          if (onPressedAction != null) {
            onPressedAction();
          }
        },
        textColor: context.primaryColor,
      ),
    ),
  );
}

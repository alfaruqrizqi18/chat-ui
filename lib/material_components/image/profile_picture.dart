import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../../material_components/extensions/context_extensions.dart';
import '../../../styles/progress_indicator.dart';

class AlphaProfilePicture extends StatelessWidget {
  final String imageUrl;
  final double size;
  final Color? errorIconColor;
  final Color? errorBackgroundColor;
  final Color? borderColor;
  final Color? progressColor;

  const AlphaProfilePicture(
      {Key? key,
      required this.imageUrl,
      this.size = 100,
      this.errorIconColor,
      this.errorBackgroundColor,
      this.borderColor,
      this.progressColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    if (imageUrl.isEmpty) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(size / 2),
        child: Container(
          width: size,
          height: size,
          color: errorBackgroundColor ?? context.disabledColor,
          child: Icon(
            Icons.account_circle_rounded,
            size: size,
            color: errorIconColor ?? context.surfaceColor,
          ),
        ),
      );
    }

    return ClipRRect(
      borderRadius: BorderRadius.circular(size / 2),
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        width: size,
        height: size,
        fit: BoxFit.cover,
        imageBuilder: (context, image) => Container(
          decoration: BoxDecoration(
              border: Border.all(
                  width: 1, color: borderColor ?? context.primaryColor),
              borderRadius: BorderRadius.circular(size / 2)),
          child: CircleAvatar(
            backgroundImage: image,
          ),
        ),
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            AdaptiveProgressIndicator(
          value: downloadProgress.progress,
          color: progressColor ?? borderColor ?? context.primaryColor,
          backgroundColor: progressColor?.withOpacity(0.3) ??
              borderColor?.withOpacity(0.3) ??
              context.surfaceVariantColor,
        ),
        errorWidget: (context, url, error) => Container(
          width: size,
          height: size,
          color: errorBackgroundColor ?? context.disabledColor,
          child: Icon(
            Icons.account_circle_rounded,
            size: size,
            color: errorIconColor ?? context.surfaceColor,
          ),
        ),
        fadeOutDuration: const Duration(seconds: 0),
        fadeInDuration: const Duration(seconds: 0),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AlphaAssetImage extends StatelessWidget {
  final String path;
  final double? height;
  final double? width;
  final BoxFit? fit;
  final Color? color;
  final double? scale;
  final AlignmentGeometry? svgAlignment;
  final Alignment? alignment;

  const AlphaAssetImage(this.path,
      {Key? key,
        this.height,
        this.width,
        this.fit,
        this.color,
        this.scale,
        this.alignment,
        this.svgAlignment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (path.endsWith("svg")) {
      return SvgPicture.asset(
        path,
        height: height,
        width: width,
        color: color,
        fit: fit ?? BoxFit.contain,
        alignment: svgAlignment ?? Alignment.center,
      );
    }

    return Image.asset(
      path,
      height: height,
      width: width,
      fit: fit,
      color: color,
      scale: scale,
      alignment: alignment ?? Alignment.center,
    );
  }
}

import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaTextField extends StatelessWidget {
  final bool obscureText;
  final bool enableSuggestions;
  final bool autocorrect;
  final bool isDense;
  final bool enabled;
  final bool readOnly;
  final bool autofocus;
  final TextEditingController? controller;
  final TextInputAction? textInputAction;
  final TextInputType? keyboardType;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSubmitted;
  final TextCapitalization textCapitalization;
  final String? hintText;
  final String? labelText;
  final String? helperText;
  final String? errorText;
  final String? prefixText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final int? helperMaxLines;
  final int? maxLines;
  final int? maxLength;
  final dynamic Function()? onTap;
  final TextStyle? hintStyle;
  final TextStyle? prefixStyle;

  const AlphaTextField(
      {Key? key,
      this.obscureText = false,
      this.enableSuggestions = false,
      this.autocorrect = false,
      this.isDense = false,
      this.enabled = true,
      this.readOnly = false,
      this.autofocus = false,
      this.controller,
      this.textInputAction,
      this.keyboardType,
      this.hintText,
      this.labelText,
      this.helperText,
      this.errorText,
      this.prefixIcon,
      this.suffixIcon,
      this.prefixText,
      this.onEditingComplete,
      this.onSubmitted,
      this.onChanged,
      this.textCapitalization = TextCapitalization.none,
      this.maxLines = 1,
      this.helperMaxLines = 4,
      this.maxLength,
      this.onTap,
      this.hintStyle,
      this.prefixStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onTap: onTap,
      obscureText: obscureText,
      enableSuggestions: enableSuggestions,
      autocorrect: autocorrect,
      enabled: enabled,
      readOnly: readOnly,
      autofocus: autofocus,
      controller: controller,
      textInputAction: textInputAction,
      keyboardType: keyboardType,
      cursorColor: context.primaryColor,
      cursorWidth: 1,
      onEditingComplete: onEditingComplete,
      onSubmitted: onSubmitted,
      onChanged: onChanged,
      textCapitalization: textCapitalization,
      maxLines: maxLines,
      maxLength: maxLength,
      style: context.getBodyLargeTextStyle(context.onSurfaceColor),
      decoration: InputDecoration(
        isDense: isDense,
        hintText: hintText,
        labelText: labelText,
        helperText: helperText,
        errorText: errorText,
        prefixText: prefixText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        helperMaxLines: helperMaxLines,
        suffixIconColor: context.onSurfaceVariantColor,
        prefixIconColor: context.onSurfaceVariantColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: context.outlineColor, width: 1, style: BorderStyle.solid),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: context.outlineColor, width: 1, style: BorderStyle.solid),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: context.primaryColor, width: 1, style: BorderStyle.solid),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: context.errorColor, width: 2, style: BorderStyle.solid),
        ),
        errorStyle: context.getBodySmallTextStyle(context.errorColor),
        helperStyle:
            context.getBodySmallTextStyle(context.onSurfaceVariantColor),
        hintStyle: hintStyle ??
            context.getBodyLargeTextStyle(context.onSurfaceVariantColor),
        //Commenting labels as their colors are determined by the state of the text field
        /*labelStyle:
            context.getBodyLargeTextStyle(context.onSurfaceVariantColor),
        floatingLabelStyle: context.getBodySmallTextStyle(context.primaryColor),*/
        prefixStyle: prefixStyle ??
            context
                .getBodyLargeTextStyle(context.onSurfaceColor.withOpacity(0.5)),
      ),
    );
  }
}

import '/material_components/buttons/text_button.dart';
import '/material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';

class AlphaMessageWidget extends StatelessWidget {
  final Widget child;
  final String title;
  final String subtitle;
  const AlphaMessageWidget({
    Key? key,
    this.child = const SizedBox(),
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            Text(
              title,
              textAlign: TextAlign.center,
              style: context
                  .getTitleLargeTextStyle(
                    context.onSurfaceColor,
                  )
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 5),
            Text(
              subtitle,
              textAlign: TextAlign.center,
              style: context.getLabelLargeTextStyle(
                context.unSelectedColor,
              ),
            ),
            const SizedBox(height: 10),
            child
          ],
        ),
      ),
    );
  }
}

Widget alphaMessageNetworkErrorWidget({required Function onRefresh}) {
  return AlphaMessageWidget(
    title: "Ada kesalahan jaringan",
    subtitle: "Periksa koneksi internet Anda terlebih dahulu ya",
    child: AlphaTextButtonIcon(
      onPressed: () {
        onRefresh();
      },
      child: "Coba lagi",
      icon: Icons.refresh,
    ),
  );
}

Widget alphaMessageUnknownErrorWidget({required Function onRefresh}) {
  return AlphaMessageWidget(
    title: "Ada kesalahan saat memuat data",
    subtitle: "Terjadi kesalahan saat memuat data",
    child: AlphaTextButtonIcon(
      onPressed: () {
        onRefresh();
      },
      child: "Coba lagi",
      icon: Icons.refresh,
    ),
  );
}

import '/material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';

class AlphaTextTitlePanel extends StatelessWidget {
  const AlphaTextTitlePanel({super.key, required this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: context.getLabelLargeTextStyle(context.unSelectedColor)?.copyWith(
          letterSpacing: 1.5, fontWeight: FontWeight.bold, fontSize: 12),
    );
  }
}

class AlphaTwoColumnTextWidget extends StatelessWidget {
  final String title;
  final dynamic value;
  final Color? valueColor;
  final CrossAxisAlignment crossAxisAlignment;
  const AlphaTwoColumnTextWidget({
    Key? key,
    required this.title,
    required this.value,
    this.valueColor,
    this.crossAxisAlignment = CrossAxisAlignment.center
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: crossAxisAlignment,
      children: [
        Expanded(
          child: AlphaTextTitlePanel(
            title: title,
          ),
        ),
        Expanded(
          child: Builder(builder: (context) {
            if (value is String) {
              return Text(
                value,
                textAlign: TextAlign.right,
                style: context.getLabelLargeTextStyle(
                  valueColor ?? context.onSurfaceColor,
                ),
              );
            }
            return value;
          }),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaFilledTonalButton extends StatelessWidget {
  final dynamic child;
  final VoidCallback? onPressed;

  const AlphaFilledTonalButton({
    Key? key,
    required this.child,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        foregroundColor: context.onSecondaryContainerColor,
        backgroundColor: context.secondaryContainerColor,
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      child: Builder(
        builder: (ctx) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
    );
  }
}

class AlphaFilledTonalButtonIcon extends StatelessWidget {
  final VoidCallback? onPressed;
  final IconData icon;
  final bool useM3RoundedStyle;
  final String label;

  const AlphaFilledTonalButtonIcon({
    Key? key,
    required this.onPressed,
    required this.icon,
    required this.label,
    this.useM3RoundedStyle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        foregroundColor: context.onSecondaryContainerColor,
        backgroundColor: context.secondaryContainerColor,
        shape: useM3RoundedStyle
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
              ),
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      icon: Icon(
        icon,
      ),
      label: Text(label),
    );
  }
}

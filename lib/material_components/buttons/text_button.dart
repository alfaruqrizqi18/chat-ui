import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaTextButton extends StatelessWidget {
  final dynamic child;
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final bool isAppBarAction;

  const AlphaTextButton({
    Key? key,
    this.padding,
    required this.onPressed,
    required this.child,
    this.isAppBarAction = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: padding,
        textStyle: isAppBarAction
            ? context.getTitleMediumTextStyle(context.primaryColor)
            : null,
      ),
      onPressed: onPressed,
      child: Builder(
        builder: (ctx) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
    );
  }
}

class AlphaTextButtonIcon extends StatelessWidget {
  final dynamic child;
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final bool isAppBarAction;
  final IconData icon;

  const AlphaTextButtonIcon({
    Key? key,
    this.padding,
    required this.onPressed,
    required this.child,
    required this.icon,
    this.isAppBarAction = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      style: TextButton.styleFrom(
        padding: padding,
        textStyle: isAppBarAction
            ? context.getTitleMediumTextStyle(context.primaryColor)
            : null,
      ),
      onPressed: onPressed,
      icon: Icon(
        icon,
        color: context.primaryColor,
        size: 20,
      ),
      label: Builder(
        builder: (ctx) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
    );
  }
}

class AlphaLittleTextButton extends StatelessWidget {
  const AlphaLittleTextButton({
    super.key,
    required this.title,
    required this.onTap,
    this.textColor,
  });
  final String title;
  final Function onTap;
  final Color? textColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      borderRadius: BorderRadius.circular(10),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 7,
          vertical: 5,
        ),
        child: Text(
          title,
          style: context.getLabelMediumTextStyle(textColor ?? context.primaryColor),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class AlphaOutlineButton extends StatelessWidget {
  final Widget child;
  final VoidCallback? onPressed;

  const AlphaOutlineButton({Key? key, required this.onPressed, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(onPressed: onPressed, child: child);
  }
}

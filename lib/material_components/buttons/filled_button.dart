import 'package:flutter/material.dart';
import '../extensions/context_extensions.dart';

class AlphaFilledButton extends StatelessWidget {
  final dynamic child;
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final bool useM3RoundedStyle;

  const AlphaFilledButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.padding,
    this.useM3RoundedStyle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        foregroundColor: context.onPrimaryColor,
        backgroundColor: context.primaryColor,
        padding: padding,
        shape: useM3RoundedStyle
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
              ),
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      child: Builder(
        builder: (ctx) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
    );
  }
}

class AlphaFilledButtonIcon extends StatelessWidget {
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final IconData icon;
  final bool useM3RoundedStyle;
  final dynamic label;

  const AlphaFilledButtonIcon({
    Key? key,
    required this.onPressed,
    this.padding,
    required this.icon,
    required this.label,
    this.useM3RoundedStyle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: context.primaryColor,
        padding: padding,
        shape: useM3RoundedStyle
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
              ),
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      icon: Icon(
        icon,
      ),
      label: Builder(builder: (context) {
        if (label is String) {
          return Text(label);
        }
        return label;
      }),
    );
  }
}

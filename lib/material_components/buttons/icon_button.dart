import 'package:chat_ui/material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';

class AlphaIconButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final IconData icon;
  final Color? color;

  const AlphaIconButton({
    Key? key,
    this.padding,
    required this.onPressed,
    required this.icon,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(
        icon,
        color: color ?? context.primaryColor,
        size: 24,
      ),
    );
  }
}

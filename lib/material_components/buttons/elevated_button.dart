import '../../material_components/extensions/context_extensions.dart';
import 'package:flutter/material.dart';

class AlphaElevatedButton extends StatelessWidget {
  final dynamic child;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry? padding;

  const AlphaElevatedButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: padding,
        backgroundColor: context.primaryColor,
        foregroundColor: context.onPrimaryColor,
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      child: Builder(
        builder: (ctx) {
          if (child is String) {
            return Text(child);
          }
          return child;
        },
      ),
    );
  }
}

class AlphaElevatedButtonIcon extends StatelessWidget {
  final VoidCallback onPressed;
  final EdgeInsetsGeometry? padding;
  final IconData icon;
  final bool useM3RoundedStyle;
  final dynamic label;

  const AlphaElevatedButtonIcon({
    Key? key,
    required this.onPressed,
    this.padding,
    required this.icon,
    required this.label,
    this.useM3RoundedStyle = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        padding: padding,
        backgroundColor: context.surfaceColor,
        foregroundColor: context.primaryColor,
        shape: useM3RoundedStyle
            ? null
            : RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      ).copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: onPressed,
      icon: Icon(
        icon,
      ),
      label: Builder(builder: (context) {
        if (label is String) {
          return Text(label);
        }
        return label;
      }),
    );
  }
}
